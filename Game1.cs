﻿using MonoGame;
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended.Graphics;
using MonoGame.Extended.Tiled;
using MonoGame.Extended.Tiled.Renderers;

namespace learn
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private Texture2D seafoamTexture;   
        private Texture2D afternoonhazeTexture;

        private Vector2 planetPosition = new Vector2(400,200);
        private Vector2 planetSize = new Vector2(16,16);
        private Body[] moons;
        private int moonCount = 8;

        TiledMap myMap;
        TiledMapRenderer mapRenderer;
        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            var viewportBounds = _graphics.GraphicsDevice.Viewport.Bounds;
            planetPosition = new Vector2((viewportBounds.Width / 2) - (planetSize.X /2),(viewportBounds.Height / 2) - (planetSize.Y /2));

            moons = new Body[moonCount];

            for (var i = 0; i < moonCount; i ++) 
            {
                moons[i] = new Body(Vector2.Zero, new Vector2(8,8), 0f, planetPosition, (float) (0.04f + ((i*0.01) + 0.11f)), 128 + (i*20));
            }

            mapRenderer = new TiledMapRenderer(_graphics.GraphicsDevice);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            seafoamTexture = Content.Load<Texture2D>("seafoam-32x");
            afternoonhazeTexture = Content.Load<Texture2D>("afternoon-haze-32x");
            myMap = Content.Load<TiledMap>("Tilemap/sample_fantasy");
            mapRenderer.LoadMap(myMap);
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
/*
            for (var i = 0; i < moonCount; i ++) 
            {
                moons[i].Update(gameTime);
            }
*/
            mapRenderer.Update(gameTime);
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here
            _spriteBatch.Begin();
            //_spriteBatch.Draw(seafoamTexture, new Vector2(16,16), Color.White);
            //_spriteBatch.Draw(afternoonhazeTexture, new Vector2(16,64), Color.White);
            // _spriteBatch.FillRectangle(planetPosition, planetSize, Color.DarkSeaGreen);
            // for (var i = 0; i < moonCount; i ++) 
            // {
            //     _spriteBatch.FillRectangle(moons[i].Position, moons[i].Size, Color.SkyBlue);
            // }
            mapRenderer.Draw();
            _spriteBatch.End();
            
            base.Draw(gameTime);
        }
    }
}
