using System;
using Microsoft.Xna.Framework;

namespace learn 
{
    public class Body
    {
        public Vector2 Position {get; set;}
        public Vector2 Size { get; set; }
        public float Angle { get; set; }
        public Vector2 Center { get; set; }
        public float RotationSpeed {get; set;}
        public int Distance { get; set;}


        public Body(Vector2 position, Vector2 size, float angle, Vector2 center, float rotationSpeed, int distance) 
        {
            Position = position;
            Size = size;
            Angle = angle;
            Center = center;
            RotationSpeed = rotationSpeed;
            Distance = distance;
        }

        public void Update(GameTime gameTime)
        {
            Position = new Vector2(Center.X + (float) Math.Cos(MathHelper.ToRadians(Angle)) * Distance,
                Center.Y + (float) Math.Sin(MathHelper.ToRadians(Angle)) * Distance);
            Angle += (float)(RotationSpeed * gameTime.ElapsedGameTime.Milliseconds);
        }

    }

}